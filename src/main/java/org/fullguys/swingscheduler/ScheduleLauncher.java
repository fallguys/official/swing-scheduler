package org.fullguys.swingscheduler;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.domain.Scheduler;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@RequiredArgsConstructor
public class ScheduleLauncher {
    private final Scheduler scheduler;

    @PostConstruct
    public void launchSchedule() {
        scheduler.generateSchedule();
    }
}
