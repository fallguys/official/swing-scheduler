package org.fullguys.swingscheduler.batch.scheduler.persistence.repository;

import org.fullguys.swingscheduler.batch.scheduler.persistence.model.BeneficiaryModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BeneficiaryRepository extends JpaRepository<BeneficiaryModel, Long> {
    List<BeneficiaryModel> findByStateAndDistrictId(String state, Long districtID);
}
