package org.fullguys.swingscheduler.batch.scheduler.algorithm.processing;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Shift;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Space;
import org.fullguys.swingscheduler.common.annotations.BatchAdapter;

import java.util.ArrayList;
import java.util.Map;

@BatchAdapter
public class SpaceFactory {
    public Space generateShiftsSpace(Map<Long, Shift> shifts) {
        return Space.builder()
                .shifts(shifts)
                .options(new ArrayList<>(shifts.keySet()))
                .build();
    }
}
