package org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence;

import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.ScheduleState;

import java.util.Optional;

public interface GetScheduleByStatePort {
    Optional<Schedule> getScheduleByState(ScheduleState scheduleState);
}
