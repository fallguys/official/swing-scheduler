package org.fullguys.swingscheduler.batch.scheduler.persistence.model;

import lombok.Data;
import org.fullguys.swingscheduler.batch.scheduler.persistence.utils.Constants;

import javax.persistence.*;
import java.time.LocalTime;

@Data
@Entity
@Table(name = "MT_WITHDRAWAL_POINT")
public class WithdrawalPointModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "withdrawal_point_id")
    private Long id;

    @Column(name = "district_id")
    private Long districtId;

    @Column(name = "weekday_hour_start")
    private LocalTime weekdayHourStart;

    @Column(name = "weekday_hour_end")
    private LocalTime weekdayHourEnd;

    @Column(name = "weekend_hour_start")
    private LocalTime weekendHourStart;

    @Column(name = "weekend_hour_end")
    private LocalTime weekendHourEnd;

    @Column(name = "infractions")
    private Long infractions;

    @Column(name = "state", length = Constants.WITHDRAWAL_POINT_STATE_LENGTH)
    private String state;
}
