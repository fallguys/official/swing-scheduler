package org.fullguys.swingscheduler.batch.scheduler.persistence.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "CT_INFO_BONUS")
public class InfoBonusModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "info_bonus_id")
    private Long id;

    @Column(name = "participant_beneficiary_id")
    private Long beneficiaryId;

    @Column(name = "participant_point_id")
    private Long withdrawalPointId;

    @Column(name = "initial_date_assigned")
    private LocalDateTime initialDateAssigned;

    @Column(name = "final_date_assigned")
    private LocalDateTime finalDateAssigned;

    @Column(name = "bonus_retired")
    private Boolean bonusRetired;

    @Column(name = "bonus_retired_date")
    private LocalDateTime bonusRetiredDate;
}
