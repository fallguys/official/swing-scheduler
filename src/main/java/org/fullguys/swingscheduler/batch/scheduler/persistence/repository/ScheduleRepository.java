package org.fullguys.swingscheduler.batch.scheduler.persistence.repository;

import org.fullguys.swingscheduler.batch.scheduler.persistence.model.ScheduleModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ScheduleRepository extends JpaRepository<ScheduleModel, Long> {
    Optional<ScheduleModel> findByState(String state);
}
