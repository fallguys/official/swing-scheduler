package org.fullguys.swingscheduler.batch.scheduler.domain.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Space;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.processing.ShiftFactory;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.processing.SpaceFactory;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.GenerateShiftsSpaceService;
import org.fullguys.swingscheduler.common.annotations.UseCase;

import java.time.LocalDate;
import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GenerateShiftsSpaceUseCase implements GenerateShiftsSpaceService {

    private final ShiftFactory shiftFactory;
    private final SpaceFactory spaceFactory;

    @Override
    public Space generateShiftsSpace(List<Benef> benefs, List<Point> points, LocalDate initialDate, LocalDate finalDate) {
        var shifts = shiftFactory.getShiftsFromPoints(points, initialDate, finalDate);
        return spaceFactory.generateShiftsSpace(shifts);
    }
}
