package org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Shift {
    private Long id;
    private Long orderInDay;
    private Long day;
    private Long point;
    private Integer capacity;

    private LocalDateTime attentionHourStart;
    private LocalDateTime attentionHourEnd;
}
