package org.fullguys.swingscheduler.batch.scheduler.domain.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.*;
import org.fullguys.swingscheduler.common.annotations.UseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

@UseCase
@RequiredArgsConstructor
public class GenerateScheduleByUbigeeUseCase implements GenerateScheduleByUbigeeService {

    private final GetPreprocessedUbigeesService getPreprocessedUbigeesService;
    private final GetPreprocessedBenefsService getPreprocessedBenefsService;
    private final GetPreprocessedPointsService getPreprocessedPointsService;
    private final CalculateFinalDateService calculateFinalDateService;
    private final GenerateShiftsSpaceService generateShiftsSpaceService;
    private final RunScheduleAlgorithmService runScheduleAlgorithmService;
    private final RegisterBonusForBeneficiariesService registerBonusForBeneficiariesService;

    Logger logger = LoggerFactory.getLogger(GenerateScheduleByUbigeeUseCase.class);

    @Override
    public LocalDate generateScheduleByUbigee(Schedule schedule) {
        var initialDate = schedule.getInitialDate();
        var ubigees = getPreprocessedUbigeesService.getPreprocessedUbigees();
        var finalDate = initialDate;

        logger.info(String.format("Start schedule generation for %d ubigees", ubigees.size()));
        for (var ubigee : ubigees) {
            var benefs = getPreprocessedBenefsService.getPreprocessedBenefs(ubigee);
            var points = getPreprocessedPointsService.getPreprocessedPoints(ubigee);

            // avoid generate schedules if no beneficiaries to assign or points to work
            if (benefs.isEmpty() || points.isEmpty()) {
                logger.info(String.format("Avoid ubigee %d with beneficiaries=%d and points=%d", ubigee.getId(), benefs.size(), points.size()));
                continue;
            }

            logger.info(String.format("Start ubigee %d with beneficiaries=%d and points=%d", ubigee.getId(), benefs.size(), points.size()));

            var possibleFinalDate = calculateFinalDateService.calculateFinalDate(benefs, points, initialDate);
            logger.info(String.format("Final date %s for ubigee %d", possibleFinalDate, ubigee.getId()));

            finalDate = checkForLatestDate(finalDate, possibleFinalDate);

            var space =  generateShiftsSpaceService.generateShiftsSpace(benefs, points, initialDate, finalDate);
            var results = runScheduleAlgorithmService.runAlgorithm(space, benefs);
            logger.info(String.format("Best result with %.10f for ubigee %d", results.getResult().getFitness(), ubigee.getId()));

            registerBonusForBeneficiariesService.registerBonusForBeneficiaries(benefs, points, space, results.getDistribution(), schedule);
        }

        return finalDate;
    }

    private LocalDate checkForLatestDate(LocalDate currentFinalDate, LocalDate possibleFinalDate) {
        if (currentFinalDate.isBefore(possibleFinalDate)) {
            return possibleFinalDate;
        }

        return currentFinalDate;
    }
}
