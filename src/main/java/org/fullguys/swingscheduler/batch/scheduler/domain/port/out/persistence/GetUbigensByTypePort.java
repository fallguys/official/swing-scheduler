package org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Ubigee;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.UbigeeType;

import java.util.List;

public interface GetUbigensByTypePort {
    List<Ubigee> getUbigensByType(UbigeeType ubigeeType);
}
