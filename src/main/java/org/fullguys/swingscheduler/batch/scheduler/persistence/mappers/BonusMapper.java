package org.fullguys.swingscheduler.batch.scheduler.persistence.mappers;

import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Bonus;
import org.fullguys.swingscheduler.batch.scheduler.persistence.model.InfoBonusModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface BonusMapper {

    @Mapping(target = "initialDateAssigned", source = "shift.attentionHourStart")
    @Mapping(target = "finalDateAssigned", source = "shift.attentionHourEnd")
    @Mapping(target = "bonusRetired", source = "hasRetired")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "beneficiaryId", source = "benef.id")                 // TODO: think of another way to set participant benef id
    @Mapping(target = "withdrawalPointId", source = "shift.point")          // TODO: think of another way to set participant point id
    @Mapping(target = "bonusRetiredDate", ignore = true)
    InfoBonusModel toInfoBonusModel(Bonus bonus);

    List<InfoBonusModel> toInfoBonusModels(List<Bonus> bonus);
}
