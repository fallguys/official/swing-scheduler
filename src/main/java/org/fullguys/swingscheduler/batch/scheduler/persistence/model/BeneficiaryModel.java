package org.fullguys.swingscheduler.batch.scheduler.persistence.model;

import lombok.Data;
import org.fullguys.swingscheduler.batch.scheduler.persistence.utils.Constants;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "MT_BENEFICIARY")
public class BeneficiaryModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beneficiary_id")
    private Long id;

    @Column(name = "district_id")
    private Long districtId;

    @Column(name = "is_older_adult")
    private Boolean isOlderAdult;

    @Column(name = "is_disabled")
    private Boolean isDisabled;

    @Column(name = "gender", length = Constants.BENEFICIARY_GENDER_LENGTH)
    private String gender;

    @Column(name = "state", length = Constants.BENEFICIARY_STATE_LENGTH)
    private String state;

    @Column(name = "hits")
    private Long hits;

    @Column(name = "infractions")
    private Long infractions;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="beneficiary_id")
    private List<WithdrawalPointChosenModel> chosenPoints;
}
