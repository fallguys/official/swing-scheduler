package org.fullguys.swingscheduler.batch.scheduler.persistence.repository;

import org.fullguys.swingscheduler.batch.scheduler.persistence.model.ParticipantBeneficiaryModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParticipantBenefRepository extends JpaRepository<ParticipantBeneficiaryModel, Long> {
}
