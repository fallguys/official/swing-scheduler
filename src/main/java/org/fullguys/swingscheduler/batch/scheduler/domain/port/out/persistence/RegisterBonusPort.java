package org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Bonus;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;

import java.util.List;

public interface RegisterBonusPort {
    void registerBonus(List<Benef> benefs, List<Point> points, List<Bonus> bonus, Schedule schedule);
}
