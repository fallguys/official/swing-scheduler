package org.fullguys.swingscheduler.batch.scheduler.pso.particles;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.pso.Factors;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.DistributionFactory;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Space;
import org.fullguys.swingscheduler.common.annotations.Algorithm;

import java.util.List;
import java.util.stream.Collectors;

@Algorithm
@RequiredArgsConstructor
public class ParticleFactory {

    private final DistributionFactory distributionFactory;

    public List<Particle> generateParticles(Space space, Integer coordSize) {
        return generateParticles(space, Factors.POPULATION, coordSize);
    }

    public List<Particle> generateParticles(Space space, Integer count, Integer coordSize) {
        var distributions = distributionFactory.generateRandomDistributions(space, coordSize, count);
        return distributions.stream().map(Particle::new).collect(Collectors.toList());
    }
}
