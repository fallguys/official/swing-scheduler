package org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence;

import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;

public interface SaveSchedulePort {
    void saveSchedule(Schedule schedule);
}
