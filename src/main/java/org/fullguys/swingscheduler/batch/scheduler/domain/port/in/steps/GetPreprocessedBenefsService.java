package org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Ubigee;

import java.util.List;

public interface GetPreprocessedBenefsService {
    List<Benef> getPreprocessedBenefs(Ubigee ubigee);
}
