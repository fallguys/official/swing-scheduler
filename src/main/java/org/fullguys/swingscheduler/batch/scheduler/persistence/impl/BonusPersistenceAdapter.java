package org.fullguys.swingscheduler.batch.scheduler.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Bonus;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.RegisterBonusPort;
import org.fullguys.swingscheduler.batch.scheduler.persistence.mappers.BonusMapper;
import org.fullguys.swingscheduler.batch.scheduler.persistence.mappers.InfoBonusRepository;
import org.fullguys.swingscheduler.batch.scheduler.persistence.mappers.ParticipantBenefMapper;
import org.fullguys.swingscheduler.batch.scheduler.persistence.mappers.ParticipantPointMapper;
import org.fullguys.swingscheduler.batch.scheduler.persistence.model.ParticipantBeneficiaryModel;
import org.fullguys.swingscheduler.batch.scheduler.persistence.model.ParticipantPointModel;
import org.fullguys.swingscheduler.batch.scheduler.persistence.repository.ParticipantBenefRepository;
import org.fullguys.swingscheduler.batch.scheduler.persistence.repository.ParticipantPointRepository;
import org.fullguys.swingscheduler.common.annotations.PersistenceAdapter;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@PersistenceAdapter
@RequiredArgsConstructor
public class BonusPersistenceAdapter implements RegisterBonusPort {

    private final ParticipantBenefRepository participantBenefRepository;
    private final ParticipantBenefMapper participantBenefMapper;

    private final ParticipantPointRepository participantPointRepository;
    private final ParticipantPointMapper participantPointMapper;

    private final InfoBonusRepository infoBonusRepository;
    private final BonusMapper bonusMapper;

    @Override
    public void registerBonus(List<Benef> benefs, List<Point> points, List<Bonus> bonus, Schedule schedule) {
        var participantBenefsRows = participantBenefMapper
                .toParticipantBeneficiaryModels(benefs)
                .stream()
                .peek(participantBeneficiaryModel -> participantBeneficiaryModel.setScheduleId(schedule.getId()))
                .collect(Collectors.toList());

        var savedBenefs = participantBenefRepository
                .saveAll(participantBenefsRows)
                .stream()
                .collect(Collectors.toMap(ParticipantBeneficiaryModel::getBeneficiaryId, Function.identity()));

        var participantPointsRows = participantPointMapper
                .toParticipantPointModels(points)
                .stream()
                .peek(participantPointModel -> participantPointModel.setScheduleId(schedule.getId()))
                .collect(Collectors.toList());

        var savedPoints = participantPointRepository
                .saveAll(participantPointsRows)
                .stream()
                .collect(Collectors.toMap(ParticipantPointModel::getWithdrawalPointId, Function.identity()));

        var bonusRows = bonusMapper
                .toInfoBonusModels(bonus)
                .stream()
                .peek(infoBonusModel -> {
                    var participantBenef = savedBenefs.get(infoBonusModel.getBeneficiaryId());
                    var participantPoint = savedPoints.get(infoBonusModel.getWithdrawalPointId());

                    infoBonusModel.setBeneficiaryId(participantBenef.getId());
                    infoBonusModel.setWithdrawalPointId(participantPoint.getId());
                })
                .collect(Collectors.toList());

        infoBonusRepository.saveAll(bonusRows);
    }
}
