package org.fullguys.swingscheduler.batch.scheduler.algorithm.utils;

import java.time.DayOfWeek;
import java.util.Iterator;

public class CircularWeek implements Iterator<DayOfWeek> {

    private int index;
    private final DayOfWeek initialDay;

    public CircularWeek(DayOfWeek initialDay) {
        this.initialDay = initialDay;
        this.index = Constants.BEFORE_ZERO;
    }

    @Override
    public boolean hasNext() {
        return Constants.ALWAYS_TRUE;
    }

    @Override
    public DayOfWeek next() {
        this.index++;
        return this.initialDay.plus(this.index);
    }

    public int getDaysPassed() {
        return this.index + Constants.ONE;
    }
}
