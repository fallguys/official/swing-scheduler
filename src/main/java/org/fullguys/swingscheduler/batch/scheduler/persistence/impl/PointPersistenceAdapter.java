package org.fullguys.swingscheduler.batch.scheduler.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.PointState;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.GetPointsByUbigeePort;
import org.fullguys.swingscheduler.batch.scheduler.persistence.mappers.PointMapper;
import org.fullguys.swingscheduler.batch.scheduler.persistence.repository.WithdrawalPointRepository;
import org.fullguys.swingscheduler.common.annotations.PersistenceAdapter;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class PointPersistenceAdapter implements GetPointsByUbigeePort {

    private final WithdrawalPointRepository withdrawalPointRepository;
    private final PointMapper pointMapper;

    @Override
    public List<Point> getPointsByUbigee(PointState pointState, Long ubigeeID) {
        var rows = withdrawalPointRepository.findAllByStateAndDistrictId(pointState.name(), ubigeeID);
        return pointMapper.toPoints(rows);
    }
}
