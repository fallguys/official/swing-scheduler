package org.fullguys.swingscheduler.batch.scheduler.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Ubigee;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.UbigeeType;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.GetUbigensByTypePort;
import org.fullguys.swingscheduler.batch.scheduler.persistence.mappers.UbigeeMapper;
import org.fullguys.swingscheduler.batch.scheduler.persistence.repository.ContagionAreaRepository;
import org.fullguys.swingscheduler.common.annotations.PersistenceAdapter;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class UbigeePersistenceAdapter implements GetUbigensByTypePort {

    private final UbigeeMapper ubigeeMapper;
    private final ContagionAreaRepository contagionAreaRepository;

    @Override
    public List<Ubigee> getUbigensByType(UbigeeType ubigeeType) {
        var contagionAreas = contagionAreaRepository.findAllByDistrict_Type(ubigeeType.name());
        return ubigeeMapper.toUbigens(contagionAreas);
    }
}
