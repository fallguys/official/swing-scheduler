package org.fullguys.swingscheduler.batch.scheduler.algorithm;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Distribution;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.function.ObjectiveResult;

@Data
@RequiredArgsConstructor
public class AlgorithmResult {
    private final Distribution distribution;
    private final ObjectiveResult result;
}
