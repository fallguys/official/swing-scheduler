package org.fullguys.swingscheduler.batch.scheduler.domain.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.Algorithm;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.AlgorithmResult;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Space;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.RunScheduleAlgorithmService;
import org.fullguys.swingscheduler.batch.scheduler.pso.PSOAlgorithm;
import org.fullguys.swingscheduler.batch.scheduler.pso.particles.ParticleFactory;
import org.fullguys.swingscheduler.common.annotations.UseCase;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class RunScheduleAlgorithmUseCase implements RunScheduleAlgorithmService {

    private final ParticleFactory particleFactory;

    @Override
    public AlgorithmResult runAlgorithm(Space space, List<Benef> beneficiaries) {
        var particles = particleFactory.generateParticles(space, beneficiaries.size());
        Algorithm algorithm = new PSOAlgorithm(space, beneficiaries, particles);
        algorithm.execute();
        return algorithm.getBestResult();
    }
}
