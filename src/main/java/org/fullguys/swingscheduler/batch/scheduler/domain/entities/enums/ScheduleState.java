package org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums;

public enum ScheduleState {
    PROCESSING,
    AVAILABLE,
    DISCARTED,
    CANCELED,
    PROGRAMMED,
    ACTIVE,
    FINISHED;
}
