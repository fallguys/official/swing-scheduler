package org.fullguys.swingscheduler.batch.scheduler.algorithm.preprocessing;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.Factors;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Ubigee;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.Constants;
import org.fullguys.swingscheduler.common.annotations.BatchAdapter;

import java.time.DayOfWeek;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;


@BatchAdapter
public class PointProcessor {

    private long calculateTotalInfractions(List<Point> points) {
        return points.stream().mapToLong(Point::getInfractions).sum();
    }

    private void calculateScore(Point point, Long totalInfractions) {
        var score =  totalInfractions > Constants.LONG_ZERO
                ? (Constants.LONG_ONE - point.getInfractions() / totalInfractions)
                : (Constants.LONG_ONE);

        point.setScore(score);
    }

    private void calculateWeekCapacity(Point point, Ubigee ubigee) {
        // init week capacity
        point.setWeekCapacity(new HashMap<>());

        // setup week capacity
        for (var day : DayOfWeek.values()) {

            if (day == DayOfWeek.SUNDAY) {
                point.getWeekCapacity().put(day, Constants.LONG_ZERO);
                continue;
            }

            var hours = day == DayOfWeek.SATURDAY
                    ? point.getWeekdayBusinessHoursStart().until(point.getWeekendBusinessHoursEnd(), ChronoUnit.HOURS)
                    : point.getWeekdayBusinessHoursStart().until(point.getWeekdayBusinessHoursEnd(), ChronoUnit.HOURS);

            var rawCapacity = hours * Constants.MINUTES_PER_HOUR / Factors.Capacity.attentionTimePerBenefInMinutes;
            var capacity = rawCapacity * ubigee.getContagionIndex() * point.getScore();

            point.getWeekCapacity().put(day, ((Double) capacity).longValue());
        }
    }

    public void preprocess(List<Point> points, Ubigee ubigee) {
        var totalInfractions = this.calculateTotalInfractions(points);

        points.forEach(point -> {
            this.calculateScore(point, totalInfractions);
            this.calculateWeekCapacity(point, ubigee);
        });
    }
}
