package org.fullguys.swingscheduler.batch.scheduler.domain.entities;

import lombok.Builder;
import lombok.Data;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Shift;

@Data
@Builder
public class Bonus {
    private Benef benef;
    private Shift shift;
    private Boolean hasRetired;
}
