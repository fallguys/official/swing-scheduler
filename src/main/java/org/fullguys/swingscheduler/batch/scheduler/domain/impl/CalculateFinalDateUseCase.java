package org.fullguys.swingscheduler.batch.scheduler.domain.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.processing.DurationProcessor;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.CalculateFinalDateService;
import org.fullguys.swingscheduler.common.annotations.UseCase;

import java.time.LocalDate;
import java.util.List;

@UseCase
@RequiredArgsConstructor
public class CalculateFinalDateUseCase implements CalculateFinalDateService {

    private final DurationProcessor durationProcessor;

    @Override
    public LocalDate calculateFinalDate(List<Benef> benefs, List<Point> points, LocalDate initialDate) {
        var startedDay = initialDate.getDayOfWeek();
        var days = durationProcessor.calculateDaysForAttention(benefs, points, startedDay);
        return initialDate.plusDays(days);
    }
}
