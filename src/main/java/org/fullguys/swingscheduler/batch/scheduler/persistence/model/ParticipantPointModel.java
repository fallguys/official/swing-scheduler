package org.fullguys.swingscheduler.batch.scheduler.persistence.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CT_PARTICIPANT_POINT")
public class ParticipantPointModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "participant_point_id")
    private Long id;

    @Column(name = "schedule_id")
    private Long scheduleId;

    @Column(name = "withdrawal_point_id")
    private Long withdrawalPointId;
}
