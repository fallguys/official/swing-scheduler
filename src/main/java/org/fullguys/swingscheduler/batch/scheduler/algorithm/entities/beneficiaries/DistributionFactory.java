package org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries;


import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Space;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.Constants;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.Randomize;
import org.fullguys.swingscheduler.common.annotations.Algorithm;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Algorithm
public class DistributionFactory {

    private List<Long> generateRandomDistributionValues(Integer length, Space space) {
        var randomize = Randomize.getInstance();
        return randomize.selectRandomList(length, space.getOptions());
    }

    public List<Distribution> generateRandomDistributions(Space space, Integer length, Integer count) {

        return IntStream.range(Constants.ZERO, count)
                .mapToObj(index -> {
                    var distribution = new Distribution();
                    distribution.setValues(generateRandomDistributionValues(length, space));
                    return distribution;
                })
                .collect(Collectors.toList());
    }
}
