package org.fullguys.swingscheduler.batch.scheduler.domain.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.ScheduleState;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.ScheduleType;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.UpdateScheduleService;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.SaveSchedulePort;
import org.fullguys.swingscheduler.common.annotations.UseCase;

import java.time.LocalDate;

@UseCase
@RequiredArgsConstructor
public class UpdateScheduleUseCase implements UpdateScheduleService {

    private final SaveSchedulePort saveSchedulePort;

    @Override
    public void updateSchedule(Schedule schedule, LocalDate finalDate) {
        var newState = schedule.getType().equals(ScheduleType.OFFICIAL)
                ? ScheduleState.PROGRAMMED
                : ScheduleState.AVAILABLE;

        schedule.setState(newState);
        schedule.setFinalDate(finalDate);
        saveSchedulePort.saveSchedule(schedule);
    }
}
