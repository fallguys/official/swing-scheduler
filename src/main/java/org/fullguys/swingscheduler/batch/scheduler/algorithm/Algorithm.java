package org.fullguys.swingscheduler.batch.scheduler.algorithm;

public interface Algorithm {
    void execute();
    AlgorithmResult getBestResult();
}
