package org.fullguys.swingscheduler.batch.scheduler.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.BeneficiaryState;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.GetBenefsByUbigeePort;
import org.fullguys.swingscheduler.batch.scheduler.persistence.mappers.BenefMapper;
import org.fullguys.swingscheduler.batch.scheduler.persistence.repository.BeneficiaryRepository;
import org.fullguys.swingscheduler.common.annotations.PersistenceAdapter;

import java.util.List;

@PersistenceAdapter
@RequiredArgsConstructor
public class BenefPersistenceAdapter implements GetBenefsByUbigeePort {

    private final BeneficiaryRepository beneficiaryRepository;
    private final BenefMapper benefMapper;

    @Override
    public List<Benef> getBenefsByUbigee(BeneficiaryState benefState, Long ubigeeID) {
        var rows = beneficiaryRepository.findByStateAndDistrictId(benefState.name(), ubigeeID);
        return benefMapper.toBenefs(rows);
    }
}
