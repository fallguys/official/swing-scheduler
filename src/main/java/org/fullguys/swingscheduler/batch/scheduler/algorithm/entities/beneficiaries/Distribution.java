package org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Distribution {
    private List<Long> values;

    public Distribution duplicate() {
        return new Distribution(new ArrayList<>(this.values));
    }

    public Integer dimension() {
        return this.values.size();
    }

    public Long diffXValue(Distribution position, int x) {
        return this.values.get(x) - position.values.get(x);
    }
}
