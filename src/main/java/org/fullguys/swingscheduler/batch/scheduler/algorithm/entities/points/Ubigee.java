package org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points;

import lombok.Data;

@Data
public class Ubigee {
    private Long id;
    private long infectedQuantity;
    private long population;

    private double contagionIndex;
}
