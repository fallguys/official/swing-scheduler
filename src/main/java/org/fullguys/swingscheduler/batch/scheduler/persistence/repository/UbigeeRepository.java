package org.fullguys.swingscheduler.batch.scheduler.persistence.repository;

import org.fullguys.swingscheduler.batch.scheduler.persistence.model.UbigeeModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UbigeeRepository extends JpaRepository<UbigeeModel, Long> {
}
