package org.fullguys.swingscheduler.batch.scheduler.pso;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.Algorithm;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.AlgorithmResult;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Distribution;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Space;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.function.ObjectiveFunction;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.function.ObjectiveResult;
import org.fullguys.swingscheduler.batch.scheduler.pso.particles.Particle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class PSOAlgorithm implements Algorithm {

    Logger logger = LoggerFactory.getLogger(PSOAlgorithm.class);

    private Distribution bestPosition;
    private ObjectiveResult bestResult;

    private final ObjectiveFunction function;

    private final Space space;
    private final List<Benef> beneficiaries;
    private final List<Particle> particles;

    public PSOAlgorithm(ObjectiveFunction function, Space space, List<Benef> beneficiaries, List<Particle> particles, ObjectiveResult bestResult) {
        this.function = function;
        this.space = space;
        this.beneficiaries = beneficiaries;
        this.particles = particles;

        this.bestResult = bestResult;
    }

    public PSOAlgorithm(Space space, List<Benef> beneficiaries, List<Particle> particles) {
        this(new ObjectiveFunction(), space, beneficiaries, particles, ObjectiveResult.init());
    }

    private void evaluateParticles() {
        particles.forEach(particle -> {
            var result = this.function.evaluate(particle.getPosition(), this.beneficiaries, this.space);
            particle.evaluate(result);

            if (particle.getBestResult().getFitness() > this.bestResult.getFitness()) {
                this.bestPosition = particle.getPosition().duplicate();
                this.bestResult = particle.getBestResult();
            }
        });
    }

    private void updateParticles() {
        particles.forEach(particle -> particle.updatePosition(this.bestPosition));
    }

    @Override
    public void execute() {
        logger.info(String.format("init PSO Algorithm execution with %d iterations", Factors.ITERATIONS));
        for (int i = 0; i < Factors.ITERATIONS; i++) {
            this.evaluateParticles();
            this.updateParticles();
            logger.info(String.format("iteration %d completed, best fitness=%.10f", i, this.bestResult.getFitness()));
        }
        logger.info(String.format("finish PSO Algorithm execution successfully with %d", Factors.ITERATIONS));
    }

    @Override
    public AlgorithmResult getBestResult() {
        return new AlgorithmResult(this.bestPosition, this.bestResult);
    }
}
