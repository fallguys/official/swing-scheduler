package org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums;

public enum BeneficiaryState {
    ACTIVE,
    INACTIVE
}
