package org.fullguys.swingscheduler.batch.scheduler.algorithm.function;

import lombok.Builder;
import lombok.Data;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.Constants;

@Data @Builder
public class ObjectiveResult {
    private Double prioritization;
    private Double agglomeration;
    private Double selection;
    private Double fitness;

    public static ObjectiveResult init() {
        return ObjectiveResult
                .builder()
                .prioritization(Constants.DOUBLE_ZERO)
                .agglomeration(Constants.DOUBLE_ZERO)
                .selection(Constants.DOUBLE_ZERO)
                .fitness(-Double.MAX_VALUE)
                .build();
    }
}
