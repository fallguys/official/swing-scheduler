package org.fullguys.swingscheduler.batch.scheduler.domain.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Ubigee;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.preprocessing.BenefProcessor;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.BeneficiaryState;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.GetPreprocessedBenefsService;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.GetBenefsByUbigeePort;
import org.fullguys.swingscheduler.common.annotations.UseCase;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetPreprocessedBenefsUseCase implements GetPreprocessedBenefsService {

    private final GetBenefsByUbigeePort getBenefsByUbigeePort;
    private final BenefProcessor benefProcessor;

    @Override
    public List<Benef> getPreprocessedBenefs(Ubigee ubigee) {
        var benefs = getBenefsByUbigeePort.getBenefsByUbigee(BeneficiaryState.ACTIVE, ubigee.getId());
        benefProcessor.preprocess(benefs);
        return benefs;
    }
}
