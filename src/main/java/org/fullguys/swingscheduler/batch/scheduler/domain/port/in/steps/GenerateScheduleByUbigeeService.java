package org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps;

import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;

import java.time.LocalDate;

public interface GenerateScheduleByUbigeeService {
    LocalDate generateScheduleByUbigee(Schedule schedule);
}
