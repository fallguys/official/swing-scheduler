package org.fullguys.swingscheduler.batch.scheduler.domain.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Ubigee;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.preprocessing.PointProcessor;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.PointState;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.GetPreprocessedPointsService;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.GetPointsByUbigeePort;
import org.fullguys.swingscheduler.common.annotations.UseCase;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetPreprocessedPointsUseCase implements GetPreprocessedPointsService {

    private final GetPointsByUbigeePort getPointsByUbigeePort;
    private final PointProcessor pointProcessor;

    @Override
    public List<Point> getPreprocessedPoints(Ubigee ubigee) {
        var points = getPointsByUbigeePort.getPointsByUbigee(PointState.ACTIVE, ubigee.getId());
        pointProcessor.preprocess(points, ubigee);
        return points;
    }
}
