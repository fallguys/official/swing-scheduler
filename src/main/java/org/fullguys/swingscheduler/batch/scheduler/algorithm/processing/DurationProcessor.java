package org.fullguys.swingscheduler.batch.scheduler.algorithm.processing;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.CircularWeek;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.Constants;
import org.fullguys.swingscheduler.common.annotations.BatchAdapter;

import java.time.DayOfWeek;
import java.util.List;

@BatchAdapter
public class DurationProcessor {

    public int calculateDaysForAttention(List<Benef> benefs, List<Point> points, DayOfWeek startedDay) {
        var demand = benefs.size();
        var capacity = Constants.LONG_ZERO;
        var week = new CircularWeek(startedDay);

        do {
            var day = week.next();
            capacity += points.stream().mapToLong(point -> point.getWeekCapacity().get(day)).sum();
        } while (demand > capacity);

        return week.getDaysPassed();
    }
}
