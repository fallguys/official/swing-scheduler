package org.fullguys.swingscheduler.batch.scheduler.domain.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Ubigee;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.preprocessing.UbigeeProcessor;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.GetDistrictUbigensService;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.GetPreprocessedUbigeesService;
import org.fullguys.swingscheduler.common.annotations.UseCase;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetPreprocessedUbigeesUseCase implements GetPreprocessedUbigeesService {

    private final GetDistrictUbigensService getDistrictUbigensService;
    private final UbigeeProcessor ubigeeProcessor;

    @Override
    public List<Ubigee> getPreprocessedUbigees() {
        var ubigees = getDistrictUbigensService.getDistrictUbigens();
        ubigeeProcessor.preprocess(ubigees);
        return ubigees;
    }
}
