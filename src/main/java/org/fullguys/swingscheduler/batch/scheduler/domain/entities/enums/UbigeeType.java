package org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums;

public enum UbigeeType {
    DEPARTMENT,
    PROVINCE,
    DISTRICT
}
