package org.fullguys.swingscheduler.batch.scheduler.algorithm.function;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.Factors;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Distribution;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Shift;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Space;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.Constants;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.Iteration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectiveFunction {

    // PRIORITY
    private Double calculatePriority(Distribution distribution, List<Benef> beneficiaries, Space space) {
        Iteration.Indexing<Long> iteration = (shiftId, benefZ) -> {
            var orderK = space.getShift(shiftId).getOrderInDay();
            var dayJ = space.getShift(shiftId).getDay();

            var favoritismByDay = 1.0 / ( (1.0+dayJ) + (1.0+benefZ) );
            var favoritismByShift = 1.0 / ( (1.0+ orderK) + (1.0+benefZ) );

            return favoritismByDay + favoritismByShift;
        };
        var priority = iteration.accumulate(distribution.getValues());

        return Factors.scalePriority.apply(priority / (2 * beneficiaries.size()));
    }

    // AGGLOMERATION
    private Double calculateAgglomeration(Distribution distribution, List<Benef> beneficiaries, Space space) {
        var agglomeration = Constants.INIT_AGGLOMERATION_VALUE;
        var filledCapacity = getCurrentFilledCapacity(distribution, space);

        Iteration.Mapping<Long, Shift> iteration = (shift, currentCapacityFilled) -> shift.getCapacity() - currentCapacityFilled;
        var capacityExceeded = iteration.accumulate(space.getShifts(), filledCapacity);

        var penalty = Factors.scaleAgglomeration.apply(((double) capacityExceeded / beneficiaries.size()));
        return agglomeration - penalty;
    }

    private Map<Long, Integer> getCurrentFilledCapacity(Distribution distribution, Space space) {
        var current = new HashMap<Long, Integer>();
        space.getShifts()
                .keySet()
                .forEach(shiftID -> current.put(shiftID, Constants.INIT_ACCUMULATED_CAPACITY_VALUE));

        distribution
                .getValues()
                .forEach(shiftID -> current.put(shiftID, current.get(shiftID)+Constants.ONE));

        return current;
    }

    // SELECTION
    private Double calculateSelection(Distribution distribution, List<Benef> beneficiaries, Space space) {
        Iteration.Comparing<Benef, Long> iteration = (benef, shiftID) -> {
            var shift = space.getShift(shiftID);
            return benef.getChosenPoints().contains(shift.getPoint());
        };

        var selection = iteration.accumulate(beneficiaries, distribution.getValues());
        return Factors.scaleSelection.apply((double) selection / beneficiaries.size());
    }

    // FITNESS EVALUATION
    public ObjectiveResult evaluate(Distribution distribution, List<Benef> beneficiaries, Space space) {
        var priority = calculatePriority(distribution, beneficiaries, space);
        var agglomeration = calculateAgglomeration(distribution, beneficiaries, space);
        var selection = calculateSelection(distribution, beneficiaries, space);

        var total = Factors.priorityWeight * priority
                + Factors.agglomerationWeight * agglomeration
                + Factors.selectionWeight * selection;

        return ObjectiveResult.builder()
                .prioritization(priority)
                .agglomeration(agglomeration)
                .selection(selection)
                .fitness(total)
                .build();
    }
}
