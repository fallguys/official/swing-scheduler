package org.fullguys.swingscheduler.batch.scheduler.persistence.repository;

import org.fullguys.swingscheduler.batch.scheduler.persistence.model.ParticipantPointModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParticipantPointRepository extends JpaRepository<ParticipantPointModel, Long> {
}
