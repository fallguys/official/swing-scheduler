package org.fullguys.swingscheduler.batch.scheduler.persistence.repository;

import org.fullguys.swingscheduler.batch.scheduler.persistence.model.ContagionAreaModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContagionAreaRepository extends JpaRepository<ContagionAreaModel, Long> {
    List<ContagionAreaModel> findAllByDistrict_Type(String ubigeeType);
}
