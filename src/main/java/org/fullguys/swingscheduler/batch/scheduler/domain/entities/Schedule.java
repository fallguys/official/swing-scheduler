package org.fullguys.swingscheduler.batch.scheduler.domain.entities;

import lombok.Data;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.ScheduleState;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.ScheduleType;

import java.time.LocalDate;

@Data
public class Schedule {
    private Long id;
    private LocalDate initialDate;
    private LocalDate finalDate;

    private ScheduleType type;
    private ScheduleState state;
}
