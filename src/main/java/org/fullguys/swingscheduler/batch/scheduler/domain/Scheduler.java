package org.fullguys.swingscheduler.batch.scheduler.domain;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.GenerateScheduleByUbigeeService;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.GetProcessingScheduleService;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.UpdateScheduleService;
import org.fullguys.swingscheduler.common.annotations.BatchAdapter;

@BatchAdapter
@RequiredArgsConstructor
public class Scheduler {

    private final GetProcessingScheduleService getProcessingScheduleService;
    private final GenerateScheduleByUbigeeService generateScheduleByUbigeeService;
    private final UpdateScheduleService updateScheduleService;

    public void generateSchedule() {
        // Get Schedules
        var schedule = getProcessingScheduleService.getProcessingSchedule();

        // sub generate schedule by ubigee and get final date
        var finalDate = generateScheduleByUbigeeService.generateScheduleByUbigee(schedule);

        // save schedule
        updateScheduleService.updateSchedule(schedule, finalDate);
    }
}
