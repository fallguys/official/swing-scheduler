package org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Distribution;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Space;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;

import java.util.List;

public interface RegisterBonusForBeneficiariesService {
    void registerBonusForBeneficiaries(List<Benef> benefs, List<Point> points, Space space, Distribution distribution, Schedule schedule);
}
