package org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Space;

import java.time.LocalDate;
import java.util.List;

public interface GenerateShiftsSpaceService {
    Space generateShiftsSpace(List<Benef> benefs, List<Point> points, LocalDate initialDate, LocalDate finalDate);
}
