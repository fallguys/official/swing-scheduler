package org.fullguys.swingscheduler.batch.scheduler.algorithm.preprocessing;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.Factors;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.Constants;
import org.fullguys.swingscheduler.common.annotations.BatchAdapter;

import java.util.Comparator;
import java.util.List;

@BatchAdapter
public class BenefProcessor {

    private void calculateScore(Benef benef) {
        var score = Factors.Benef.hitWeight * benef.getHits()
                - Factors.Benef.infractionWeight * benef.getInfractions();

        benef.setScore(score);
    }

    private void calculatePriority(Benef benef) {
        var priority = (benef.isGender() ? Factors.Benef.genderWeight   : Constants.ZERO)
                + (benef.isDisabled()    ? Factors.Benef.disabledWeight : Constants.ZERO)
                + (benef.isElder()       ? Factors.Benef.elderWeight    : Constants.ZERO)
                + benef.getScore();

        benef.setPriority(priority);
    }

    public void preprocess(List<Benef> benefs) {
        benefs.forEach(benef -> {
            this.calculateScore(benef);
            this.calculatePriority(benef);
        });

        benefs.sort(Comparator.comparingInt(Benef::getScore).reversed());
    }
}
