package org.fullguys.swingscheduler.batch.scheduler.persistence.mappers;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.persistence.model.ParticipantBeneficiaryModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface ParticipantBenefMapper {

    @Mapping(target = "beneficiaryId", source = "id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "scheduleId", ignore = true)
    ParticipantBeneficiaryModel toParticipantBeneficiaryModel(Benef benef);

    List<ParticipantBeneficiaryModel> toParticipantBeneficiaryModels(List<Benef> benefs);
}
