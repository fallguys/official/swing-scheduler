package org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Ubigee;

import java.util.List;

public interface GetPreprocessedPointsService {
    List<Point> getPreprocessedPoints(Ubigee ubigee);
}
