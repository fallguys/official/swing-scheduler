package org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.BeneficiaryState;

import java.util.List;

public interface GetBenefsByUbigeePort {
    List<Benef> getBenefsByUbigee(BeneficiaryState benefState, Long ubigeeID);
}
