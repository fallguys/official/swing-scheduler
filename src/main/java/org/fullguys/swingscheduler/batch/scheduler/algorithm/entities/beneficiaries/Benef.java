package org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries;

import lombok.Data;

import java.util.List;

@Data
public class Benef {
    private Long id;
    private List<Long> chosenPoints;
    private boolean gender;
    private boolean disabled;
    private boolean elder;
    private int infractions;
    private int hits;

    private int priority;
    private int score;
}
