package org.fullguys.swingscheduler.batch.scheduler.domain.errors;

public class ScheduleNotFoundError extends RuntimeException {
    public ScheduleNotFoundError() {
        super("No existe ningún cronograma procesándose");
    }
}
