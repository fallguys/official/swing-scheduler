package org.fullguys.swingscheduler.batch.scheduler.persistence.mappers;

import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;
import org.fullguys.swingscheduler.batch.scheduler.persistence.model.ScheduleModel;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel="spring")
public interface ScheduleMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "initialDate", source = "initialDate")
    @Mapping(target = "finalDate", source = "finalDate")
    @Mapping(target = "state", source = "state")
    @Mapping(target = "type", source = "type")
    Schedule toSchedule(ScheduleModel scheduleModel);

    @InheritInverseConfiguration
    ScheduleModel toScheduleModel(Schedule schedule);
}
