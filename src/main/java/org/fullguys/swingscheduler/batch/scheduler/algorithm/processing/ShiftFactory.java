package org.fullguys.swingscheduler.batch.scheduler.algorithm.processing;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.Factors;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Shift;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.Constants;
import org.fullguys.swingscheduler.common.annotations.BatchAdapter;
import org.javatuples.Triplet;

import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

@BatchAdapter
public class ShiftFactory {

    private Integer calculateShiftCapacity(LocalTime initialTime, LocalTime endTime, long capacityPerHour) {
        return (int)(Duration.between(initialTime, endTime).toHours() * capacityPerHour);
    }

    private List<Triplet<LocalTime, LocalTime, Integer>> getIntervalsWithCapacity(LocalTime initialHour, LocalTime endHour, long capacityPerHour) {
        var intervalsNumber = Duration.between(initialHour, endHour).toHours() / Factors.Shift.hoursPerShift;
        return IntStream.range(Constants.ZERO, (int) intervalsNumber)
                .mapToObj(index -> {
                    var initIntervalHour = initialHour.plusHours((long) index * Factors.Shift.hoursPerShift);

                    var possibleEndIntervalHour = initialHour.plusHours((long) (index + Constants.ONE) * Factors.Shift.hoursPerShift);
                    var endIntervalHour = possibleEndIntervalHour.isAfter(endHour) ? endHour : possibleEndIntervalHour;

                    var intervalCapacity = calculateShiftCapacity(initialHour, endHour, capacityPerHour);

                    return Triplet.with(initIntervalHour, endIntervalHour, intervalCapacity);
                })
                .collect(Collectors.toList());
    }

    private List<Shift> getShiftsForDayInPoint(Point point, LocalDate initialDate, Long dayNumber) {
        var currentDate = initialDate.plusDays(dayNumber);
        var dayOfWeek = currentDate.getDayOfWeek();

        var attentionStartTime = point.getBusinessHoursStart(dayOfWeek);
        var attentionEndTime = point.getBusinessHoursEnd(dayOfWeek);
        var pointCapacity = point.getWeekCapacity().get(dayOfWeek);
        var capacityPerHour = pointCapacity / Duration.between(attentionStartTime, attentionEndTime).toHours();

        var intervalsWithCapacity = getIntervalsWithCapacity(attentionStartTime, attentionEndTime, capacityPerHour);

        return IntStream.range(Constants.ZERO, intervalsWithCapacity.size())
                .mapToObj(index -> {
                    var interval = intervalsWithCapacity.get(index);
                    var shiftAttentionStart = currentDate.atTime(interval.getValue0());
                    var shiftAttentionEnd = currentDate.atTime(interval.getValue1());

                    return Shift.builder()
                            .attentionHourStart(shiftAttentionStart)
                            .attentionHourEnd(shiftAttentionEnd)
                            .capacity(interval.getValue2())
                            .day(dayNumber)
                            .point(point.getId())
                            .orderInDay((long) index)
                            .build();
                })
                .collect(Collectors.toList());
    }

    private List<Shift> defineShiftsForPoint(Point point, LocalDate initialDate, LocalDate finalDate) {
        var shifts = new ArrayList<Shift>();
        var duration = ChronoUnit.DAYS.between(initialDate, finalDate) + Constants.LONG_COUNT_TODAY;

        LongStream.range(Constants.LONG_ZERO, duration)
                .boxed()
                .forEach(dayNumber -> {
                    var shiftsInDay = getShiftsForDayInPoint(point, initialDate, dayNumber);
                    shifts.addAll(shiftsInDay);
                });

        return shifts;
    }

    public Map<Long, Shift> getShiftsFromPoints(List<Point> points, LocalDate initialDate, LocalDate finalDate) {
        var shifts = new ArrayList<Shift>();

        points.forEach(point -> {
            var currentShifts = defineShiftsForPoint(point, initialDate, finalDate);
            shifts.addAll(currentShifts);
        });

        var mapped = new HashMap<Long, Shift>();
        LongStream.range(Constants.LONG_ZERO, shifts.size())
                .boxed()
                .forEach(index -> mapped.put(index, shifts.get(index.intValue())));
        return mapped;
    }
}
