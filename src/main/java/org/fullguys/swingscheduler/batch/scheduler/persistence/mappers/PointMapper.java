package org.fullguys.swingscheduler.batch.scheduler.persistence.mappers;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.persistence.model.WithdrawalPointModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface PointMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "ubigeeID", source = "districtId")
    @Mapping(target = "weekdayBusinessHoursStart", source = "weekdayHourStart")
    @Mapping(target = "weekdayBusinessHoursEnd", source = "weekdayHourEnd")
    @Mapping(target = "weekendBusinessHoursStart", source = "weekendHourStart")
    @Mapping(target = "weekendBusinessHoursEnd", source = "weekendHourEnd")
    @Mapping(target = "infractions", ignore = true)
    @Mapping(target = "weekCapacity", ignore = true)
    @Mapping(target = "score", ignore = true)
    Point toPoint(WithdrawalPointModel withdrawalPointModel);

    List<Point> toPoints(List<WithdrawalPointModel> withdrawalPointModels);
}
