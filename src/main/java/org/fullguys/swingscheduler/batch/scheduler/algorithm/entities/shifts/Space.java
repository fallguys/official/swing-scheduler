package org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@Builder
public class Space {
    private final List<Long> options;
    private final Map<Long, Shift> shifts;

    public Shift getShift(Long id) {
        return this.shifts.get(id);
    }
}
