package org.fullguys.swingscheduler.batch.scheduler.persistence.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "MT_CONTAGION_AREA")
public class ContagionAreaModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contageon_area_id")
    private Long id;

    @Column(name = "district_id")
    private Long districtId;

    @Column(name = "quantity")
    private Long quantity;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @Column(name = "population")
    private Long population;

    @OneToOne
    @JoinColumn(name = "district_id", insertable = false, updatable = false)
    private UbigeeModel district;

}
