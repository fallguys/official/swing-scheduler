package org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points;

import lombok.Data;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Map;

@Data
public class Point {
    private Long id;
    private Long ubigeeID;

    private long infractions;
    private LocalTime WeekdayBusinessHoursStart;
    private LocalTime WeekdayBusinessHoursEnd;
    private LocalTime WeekendBusinessHoursStart;
    private LocalTime WeekendBusinessHoursEnd;

    private Map<DayOfWeek, Long> weekCapacity;
    private double score;

    public LocalTime getBusinessHoursStart(DayOfWeek day) {
        if (day == DayOfWeek.SATURDAY || day == DayOfWeek.SUNDAY) {
            return this.WeekendBusinessHoursStart;
        }
        return this.WeekdayBusinessHoursStart;
    }

    public LocalTime getBusinessHoursEnd(DayOfWeek day) {
        if (day == DayOfWeek.SATURDAY || day == DayOfWeek.SUNDAY) {
            return this.WeekendBusinessHoursEnd;
        }
        return this.WeekdayBusinessHoursEnd;
    }
}
