package org.fullguys.swingscheduler.batch.scheduler.algorithm.utils;

import java.util.List;
import java.util.Map;

public class Iteration {
    @FunctionalInterface
    public interface Indexing<E> {
        Double indexed(E element, Integer index);

        default Double accumulate(List<E> elements) {
            var accumulate = 0.0;
            for (int i = 0; i < elements.size(); i++) {
                accumulate += indexed(elements.get(i), i);
            }
            return accumulate;
        }
    }

    @FunctionalInterface
    public interface Mapping<ID, T> {
        Integer mapped(T element, Integer subtrahend);

        default Integer accumulate(Map<ID, T> elements, Map<ID, Integer> subtrahends) {
            var accumulate = 0;
            for (var key : elements.keySet()) {
                accumulate += mapped(elements.get(key), subtrahends.get(key));
            }
            return accumulate;
        }
    }

    @FunctionalInterface
    public interface Comparing<C1, C2> {
        Boolean compared(C1 e1, C2 e2);

        default Integer accumulate(List<C1> es1, List<C2> es2) {
            var accumulate = 0;
            for (int i = 0; i < es1.size(); i++) {
                accumulate += compared(es1.get(i), es2.get(i))
                        ? Constants.ONE
                        : Constants.ZERO;
            }

            return accumulate;
        }
    }
}
