package org.fullguys.swingscheduler.batch.scheduler.pso.particles;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Distribution;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.function.ObjectiveResult;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.Randomize;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Particle {

    private Distribution position;
    private ObjectiveResult result;

    private Distribution bestPosition;
    private ObjectiveResult bestResult;

    public Particle(Distribution position) {
        this.position = position;
        this.bestResult = ObjectiveResult.init();
    }

    public void evaluate(ObjectiveResult result) {
        if (result.getFitness() > this.bestResult.getFitness()) {
            this.bestPosition = this.position.duplicate();
            this.bestResult = result;
        }
    }

    public void updatePosition(Distribution bestGlobalPosition) {
        var dimension = this.position.dimension();
        var randomize = Randomize.getInstance();
        var randomPossibility1 = randomize.generateRandomList(dimension);
        var randomPossibility2 = randomize.generateRandomList(dimension);

        for (int x = 0; x < dimension; x++) {
            var chooseSocialLearning = randomPossibility1.get(x) >= randomPossibility2.get(x);
            var velocityX = chooseSocialLearning
                    ? bestGlobalPosition.diffXValue(this.position, x)
                    : this.bestPosition.diffXValue(this.position, x);

            var posX = this.position.getValues().get(x) + randomize.zeroOrOne() * velocityX;
            this.position.getValues().set(x, posX);
        }
    }
}
