package org.fullguys.swingscheduler.batch.scheduler.domain.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Ubigee;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.UbigeeType;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.GetDistrictUbigensService;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.GetUbigensByTypePort;
import org.fullguys.swingscheduler.common.annotations.UseCase;

import java.util.List;

@UseCase
@RequiredArgsConstructor
public class GetDistrictUbigensUseCase implements GetDistrictUbigensService {

    private final GetUbigensByTypePort getUbigensByTypePort;

    @Override
    public List<Ubigee> getDistrictUbigens() {
        return getUbigensByTypePort.getUbigensByType(UbigeeType.DISTRICT);
    }
}
