package org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.AlgorithmResult;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Space;

import java.util.List;

public interface RunScheduleAlgorithmService {
    AlgorithmResult runAlgorithm(Space space, List<Benef> beneficiaries);
}
