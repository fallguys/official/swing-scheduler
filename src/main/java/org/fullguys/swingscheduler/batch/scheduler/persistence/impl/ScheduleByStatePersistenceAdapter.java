package org.fullguys.swingscheduler.batch.scheduler.persistence.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.ScheduleState;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.GetScheduleByStatePort;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.SaveSchedulePort;
import org.fullguys.swingscheduler.batch.scheduler.persistence.mappers.ScheduleMapper;
import org.fullguys.swingscheduler.batch.scheduler.persistence.repository.ScheduleRepository;
import org.fullguys.swingscheduler.common.annotations.PersistenceAdapter;

import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class ScheduleByStatePersistenceAdapter implements GetScheduleByStatePort, SaveSchedulePort {

    private final ScheduleRepository scheduleRepository;
    private final ScheduleMapper scheduleMapper;

    @Override
    public Optional<Schedule> getScheduleByState(ScheduleState scheduleState) {
        var row = scheduleRepository.findByState(scheduleState.name());
        return row.map(scheduleMapper::toSchedule);
    }

    @Override
    public void saveSchedule(Schedule schedule) {
        var row = scheduleMapper.toScheduleModel(schedule);
        scheduleRepository.save(row);
    }
}
