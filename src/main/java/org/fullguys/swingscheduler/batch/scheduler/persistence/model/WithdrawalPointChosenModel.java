package org.fullguys.swingscheduler.batch.scheduler.persistence.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "CT_WITHDRAWAL_POINT_CHOOSED")
public class WithdrawalPointChosenModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "withdrawal_point_choosed_id")
    private Long id;

    @Column(name = "beneficiary_id")
    private Long beneficiaryId;

    @Column(name = "withdrawal_point_id")
    private Long withdrawalPointId;
}
