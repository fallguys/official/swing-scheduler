package org.fullguys.swingscheduler.batch.scheduler.persistence.mappers;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Ubigee;
import org.fullguys.swingscheduler.batch.scheduler.persistence.model.ContagionAreaModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface UbigeeMapper {

    @Mapping(target = "id", source = "district.id")
    @Mapping(target = "infectedQuantity", source = "quantity")
    @Mapping(target = "population", source = "population")
    @Mapping(target = "contagionIndex", ignore = true)
    Ubigee toUbigee(ContagionAreaModel contagionAreaModel);

    List<Ubigee> toUbigens(List<ContagionAreaModel> contagionAreaModels);
}
