package org.fullguys.swingscheduler.batch.scheduler.persistence.model;

import lombok.Data;
import org.fullguys.swingscheduler.batch.scheduler.persistence.utils.Constants;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "MT_SCHEDULE")
public class ScheduleModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedule_id")
    private Long id;

    @Column(name = "initial_date")
    private LocalDate initialDate;

    @Column(name = "final_date")
    private LocalDate finalDate;

    @Column(name = "type", length = Constants.SCHEDULE_TYPE_LENGTH)
    private String type;

    @Column(name = "state", length = Constants.SCHEDULE_STATE_LENGTH)
    private String state;
}
