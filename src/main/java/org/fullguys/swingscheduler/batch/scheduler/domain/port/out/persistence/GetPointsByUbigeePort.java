package org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.PointState;

import java.util.List;

public interface GetPointsByUbigeePort {
    List<Point> getPointsByUbigee(PointState pointState, Long ubigeeID);
}
