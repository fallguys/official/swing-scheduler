package org.fullguys.swingscheduler.batch.scheduler.algorithm;

import java.util.function.Function;

public class Factors {
    public static int priorityWeight = 20;
    public static int agglomerationWeight = 40;
    public static int selectionWeight = 40;

    public static Function<Double, Double> scalePriority = priority -> priority;
    public static Function<Double, Double> scaleAgglomeration = Math::cbrt;
    public static Function<Double, Double> scaleSelection = selection -> selection;

    public static class Benef {

        public static final int infractionWeight = 1;
        public static final int hitWeight = 2;

        public static final int genderWeight = 1;
        public static final int disabledWeight = 5;
        public static final int elderWeight = 10;
    }

    public static class Capacity {
        public static final int attentionTimePerBenefInMinutes = 3;
    }

    public static class Shift {
        public static final int hoursPerShift = 3;
        public static final long initialOrderInDay = 0;
    }
}
