package org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps;

import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;

public interface GetProcessingScheduleService {
    Schedule getProcessingSchedule();
}
