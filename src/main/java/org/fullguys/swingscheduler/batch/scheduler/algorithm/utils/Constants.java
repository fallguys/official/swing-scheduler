package org.fullguys.swingscheduler.batch.scheduler.algorithm.utils;

public class Constants {
    public static final double INIT_FITNESS_VALUE = 0.0;
    public static final double INIT_PRIORITY_VALUE = 0.0;
    public static final double INIT_AGGLOMERATION_VALUE = 1.0;
    public static final double INIT_SELECTION_VALUE = 0.0;

    public static final int INIT_ACCUMULATED_CAPACITY_VALUE = 0;

    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;

    public static final long LONG_ZERO = 0;
    public static final long LONG_ONE = 1;

    public static final double DOUBLE_ZERO = 0.0;
    public static final double DOUBLE_ONE = 1.0;

    public static final long MINUTES_PER_HOUR = 60;

    public static final boolean ALWAYS_TRUE = true;
    public static final int BEFORE_ZERO = -1;
    public static final long LONG_COUNT_TODAY = 1;

    public static final double MIN_NEGATIVE_DOUBLE = -Double.MAX_VALUE;
}
