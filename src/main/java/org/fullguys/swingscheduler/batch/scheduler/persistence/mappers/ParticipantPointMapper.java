package org.fullguys.swingscheduler.batch.scheduler.persistence.mappers;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.persistence.model.ParticipantPointModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel="spring")
public interface ParticipantPointMapper {

    @Mapping(target = "withdrawalPointId", source = "id")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "scheduleId", ignore = true)
    ParticipantPointModel toParticipantPointModel(Point point);

    List<ParticipantPointModel> toParticipantPointModels(List<Point> points);
}
