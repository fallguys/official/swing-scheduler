package org.fullguys.swingscheduler.batch.scheduler.algorithm.utils;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Randomize {

    private final Random random;
    private static Randomize randomize;

    private Randomize() {
        this.random = new Random();
    }

    public static Randomize getInstance() {
        if (randomize == null) {
            Randomize.randomize = new Randomize();
        }

        return Randomize.randomize;
    }

    public List<Double> generateRandomList(Integer length) {
        return random
                .doubles(length)
                .boxed()
                .collect(Collectors.toList());
    }

    public List<Long> selectRandomList(Integer size, List<Long> alternatives) {
        if (alternatives == null || alternatives.isEmpty()) {
            throw new IllegalArgumentException("alternatives list cannot be null or empty");
        }

        var randomIndex = random
                .ints(size, Constants.ZERO, alternatives.size())
                .boxed()
                .collect(Collectors.toList());

        return randomIndex
                .stream()
                .map(alternatives::get)
                .collect(Collectors.toList());
    }

    public long zeroOrOne() {
        return random.nextInt(Constants.TWO);
    }
}
