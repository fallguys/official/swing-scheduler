package org.fullguys.swingscheduler.batch.scheduler.persistence.mappers;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.persistence.model.BeneficiaryModel;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.Gender;
import org.fullguys.swingscheduler.batch.scheduler.persistence.model.WithdrawalPointChosenModel;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel="spring")
public interface BenefMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "disabled", source = "isDisabled")
    @Mapping(target = "elder", source = "isOlderAdult")
    @Mapping(target = "gender", ignore = true)
    @Mapping(target = "hits", source = "hits")
    @Mapping(target = "infractions", source = "infractions")
    @Mapping(target = "chosenPoints", ignore = true)
    @Mapping(target = "priority", ignore = true)
    @Mapping(target = "score", ignore = true)
    Benef toBenef(BeneficiaryModel beneficiaryModel);

    List<Benef> toBenefs(List<BeneficiaryModel> beneficiaryModels);

    @AfterMapping
    default void setCustomProperties(BeneficiaryModel beneficiaryModel, @MappingTarget Benef benef) {
        // gender
        var gender = Gender.MALE.getValue().equals(beneficiaryModel.getGender());
        benef.setGender(gender);

        // chosenPoints
        var points = beneficiaryModel
                .getChosenPoints()
                .stream()
                .map(WithdrawalPointChosenModel::getWithdrawalPointId)
                .collect(Collectors.toList());
        benef.setChosenPoints(points);
    }

}
