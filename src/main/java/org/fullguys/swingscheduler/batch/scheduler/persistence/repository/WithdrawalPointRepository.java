package org.fullguys.swingscheduler.batch.scheduler.persistence.repository;

import org.fullguys.swingscheduler.batch.scheduler.persistence.model.WithdrawalPointModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WithdrawalPointRepository extends JpaRepository<WithdrawalPointModel, Long> {
    List<WithdrawalPointModel> findAllByStateAndDistrictId(String state, Long districtID);
}
