package org.fullguys.swingscheduler.batch.scheduler.domain.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.enums.ScheduleState;
import org.fullguys.swingscheduler.batch.scheduler.domain.errors.ScheduleNotFoundError;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.GetProcessingScheduleService;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.GetScheduleByStatePort;
import org.fullguys.swingscheduler.common.annotations.UseCase;

@UseCase
@RequiredArgsConstructor
public class GetProcessingScheduleUseCase implements GetProcessingScheduleService {

    private final GetScheduleByStatePort getScheduleByStatePort;

    @Override
    public Schedule getProcessingSchedule() {
        return getScheduleByStatePort
                .getScheduleByState(ScheduleState.PROCESSING)
                .orElseThrow(ScheduleNotFoundError::new);
    }
}
