package org.fullguys.swingscheduler.batch.scheduler.algorithm.preprocessing;

import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Ubigee;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.utils.Constants;
import org.fullguys.swingscheduler.common.annotations.BatchAdapter;

import java.util.List;

@BatchAdapter
public class UbigeeProcessor {

    private void calculateContagionIndex(Ubigee ubigee) {
        var index = ubigee.getPopulation() > Constants.DOUBLE_ZERO
                ? (Constants.DOUBLE_ONE - ubigee.getInfectedQuantity() / (double) ubigee.getPopulation())
                : (Constants.DOUBLE_ONE);
        ubigee.setContagionIndex(index);
    }

    public void preprocess(List<Ubigee> ubigens) {
        ubigens.forEach(this::calculateContagionIndex);
    }
}
