package org.fullguys.swingscheduler.batch.scheduler.domain.impl;

import lombok.RequiredArgsConstructor;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Benef;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.beneficiaries.Distribution;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.points.Point;
import org.fullguys.swingscheduler.batch.scheduler.algorithm.entities.shifts.Space;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Bonus;
import org.fullguys.swingscheduler.batch.scheduler.domain.entities.Schedule;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.in.steps.RegisterBonusForBeneficiariesService;
import org.fullguys.swingscheduler.batch.scheduler.domain.port.out.persistence.RegisterBonusPort;
import org.fullguys.swingscheduler.common.annotations.UseCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@UseCase
@RequiredArgsConstructor
public class RegisterBonusForBeneficiariesUseCase implements RegisterBonusForBeneficiariesService {

    private final RegisterBonusPort registerBonusPort;
    Logger logger = LoggerFactory.getLogger(RegisterBonusForBeneficiariesUseCase.class);

    @Override
    public void registerBonusForBeneficiaries(List<Benef> benefs, List<Point> points, Space space, Distribution distribution, Schedule schedule) {
        logger.info(String.format("Registering bonus for %d beneficiaries and %d withdrawal points", benefs.size(), points.size()));
        var bonusList = new ArrayList<Bonus>();

        for (int i=0; i<distribution.dimension(); i++) {
            var benef = benefs.get(i);
            var shift = space.getShift(distribution.getValues().get(i));
            var bonus = Bonus.builder()
                    .benef(benef)
                    .shift(shift)
                    .hasRetired(false)
                    .build();
            bonusList.add(bonus);
        }

        registerBonusPort.registerBonus(benefs, points, bonusList, schedule);
        logger.info(String.format("Bonus registered successfully for schedule %d", schedule.getId()));
    }
}
