package org.fullguys.swingscheduler.batch.scheduler.persistence.mappers;

import org.fullguys.swingscheduler.batch.scheduler.persistence.model.InfoBonusModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InfoBonusRepository extends JpaRepository<InfoBonusModel, Long> {
}
